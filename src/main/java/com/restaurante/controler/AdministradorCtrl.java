package com.restaurante.controler;

import com.restaurante.model.entity.Administrador;
import com.restaurante.model.repositori.IAdministradorDao;
import com.restaurante.model.services.implement.AdministradorServices;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/administrador")
public class AdministradorCtrl implements AdministradorServices{
    @Autowired
    private IAdministradorDao administradorDao;
    private Administrador admin;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/administrador/{id}")
    @ApiOperation(value="crea un administrador",notes="crea un administrador"
                  ,response = Administrador.class,produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operación Exitosa", response = Administrador.class),
            @ApiResponse(code = 401, message = "No posees  autorización"),
            @ApiResponse(code = 403, message = "Esta operación no esta permitida"),
            @ApiResponse(code = 404, message = "Recurso no encotrado"),
            @ApiResponse(code = 500, message = "Error del sistema"),
            @ApiResponse(code = 400,message = "null",response = Administrador.class)
    })
    public Administrador crearAdministrador(@PathVariable long id){


        return administradorDao.save(admin);
    }

    @Override
    public Administrador crearAdministrador(long identificacion, String nombre) {
        return null;
    }

    @Override
    public Administrador modificarAdministrador(long id, String nombre) {
        return null;
    }

    @Override
    public void eliminarAdministrador() {

    }

    @Override
    public Administrador mostrarAdministrador() {
        return null;
    }


/*
    @GetMapping("/administrador")
    @ApiOperation(value = "lista todos los administradores", notes = "<br><i> Retorna una lista de todos los administradores del restaurante</i>"
            , response = Administrador[].class, responseContainer = "List",produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operación Exitosa", response = Administrador[].class),
            @ApiResponse(code = 401, message = "No posees  autorización"),
            @ApiResponse(code = 403, message = "Esta operación no esta permitida"),
            @ApiResponse(code = 404, message = "Recurso no encotrado"),
            @ApiResponse(code = 500, message = "Error del sistema")
    })
    public List<Administrador> listar() throws MasterResourceNotFoundException {
        return administradorServices.findAll();
    }

    @GetMapping("/administrador/{id}")
    @ApiOperation(value = "Retorna un administradores", notes = "<br> retorna un administrador por id"
            , response = Administrador[].class, responseContainer = "List",produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operación Exitosa", response = Administrador[].class),
            @ApiResponse(code = 401, message = "No posees  autorización"),
            @ApiResponse(code = 403, message = "Esta operación no esta permitida"),
            @ApiResponse(code = 404, message = "Recurso no encotrado"),
            @ApiResponse(code = 500, message = "Error del sistema")
    })
    public Administrador ver(@PathVariable Integer id)  {
        return administradorServices.findById(id);
}


    @PostMapping("/administrador/{identificacion}")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "modifica un administrador", notes = "<br> modifica un administrador de restaurante"
            , response = Administrador[].class, responseContainer = "List",produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operación Exitosa", response = Administrador[].class),
            @ApiResponse(code = 401, message = "No posees  autorización"),
            @ApiResponse(code = 403, message = "Esta operación no esta permitida"),
            @ApiResponse(code = 404, message = "Recurso no encotrado"),
            @ApiResponse(code = 500, message = "Error del sistema")
    })
    public Administrador crearAdministrador(@PathVariable long id){
          a.setNombre("..");
          a.setIdentificacion(id);

        return administradorServices.save(a);
    }

    @PutMapping("/Platillo")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "agrega un platillo al restaurante", notes = "<br>agrega un platillo"
            , response = Administrador[].class, responseContainer = "List",produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operación Exitosa", response = Administrador[].class),
            @ApiResponse(code = 401, message = "No posees  autorización"),
            @ApiResponse(code = 403, message = "Esta operación no esta permitida"),
            @ApiResponse(code = 404, message = "Recurso no encotrado"),
            @ApiResponse(code = 500, message = "Error del sistema")
    })
    public Administrador agregarPlatillo(@RequestBody Administrador a, Platillo p){
        a.getRestaurante().getPlatillos().add(p) ;
        return administradorServices.save(a);
    }


    @PutMapping("/administrador/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "modifica un administrador", notes = "<br> modifica un administrador de restaurante"
            , response = Administrador[].class, responseContainer = "List",produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operación Exitosa", response = Administrador[].class),
            @ApiResponse(code = 401, message = "No posees  autorización"),
            @ApiResponse(code = 403, message = "Esta operación no esta permitida"),
            @ApiResponse(code = 404, message = "Recurso no encotrado"),
            @ApiResponse(code = 500, message = "Error del sistema")
    })
    public Administrador actualizar(@RequestBody Administrador a, @PathVariable long id) {
        Administrador mAdministrador =administradorServices.findById(id);
        mAdministrador.setNombre(a.getNombre());

        return administradorServices.save(mAdministrador);
    }

    @DeleteMapping("/adiministrador/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = "elimina un administrador", notes = "<br> elimina un administrador de tienda"
            , response = Administrador[].class, responseContainer = "List",produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operación Exitosa", response = Administrador[].class),
            @ApiResponse(code = 401, message = "No posees  autorización"),
            @ApiResponse(code = 403, message = "Esta operación no esta permitida"),
            @ApiResponse(code = 404, message = "Recurso no encotrado"),
            @ApiResponse(code = 500, message = "Error del sistema")
    })
    public void eliminar(@PathVariable long id){
        administradorServices.delete(id );
    }
*/
}
