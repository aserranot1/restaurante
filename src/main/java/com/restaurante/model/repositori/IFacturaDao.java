package com.restaurante.model.repositori;

import com.restaurante.model.entity.Factura;
import org.springframework.data.repository.CrudRepository;

public interface IFacturaDao extends CrudRepository<Factura,Integer> {
}
