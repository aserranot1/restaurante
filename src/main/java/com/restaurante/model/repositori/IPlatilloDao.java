package com.restaurante.model.repositori;

import com.restaurante.model.entity.Platillo;
import org.springframework.data.repository.CrudRepository;

public interface IPlatilloDao extends CrudRepository<Platillo,Integer> {
}
