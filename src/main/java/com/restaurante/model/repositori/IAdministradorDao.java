package com.restaurante.model.repositori;

import com.restaurante.model.entity.Administrador;
import org.springframework.data.repository.CrudRepository;

public interface IAdministradorDao extends CrudRepository<Administrador,Long> {
}
