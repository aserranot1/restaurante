package com.restaurante.model.repositori;

import com.restaurante.model.entity.Restaurante;
import org.springframework.data.repository.CrudRepository;

public interface IRestauranteDao extends CrudRepository<Restaurante,Integer> {
}
