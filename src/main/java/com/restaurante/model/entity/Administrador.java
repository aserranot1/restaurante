package com.restaurante.model.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table( name ="administrador")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Administrador implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true)
    private long identificacion;

    @Column(name = "Administrador")
    private String nombre;


    private static final long serialVersionUID = -7906047055409459029L;

}
