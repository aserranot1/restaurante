package com.restaurante.model.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name="restaurantes")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Restaurante implements Serializable {

    @Id
    @Column(name = "idRestaurante")
    private int idRestaurante;

    @Column(name= "restaurante")
    private String nombreRestaurante;

    private String sede;

    @OneToMany
    @JoinColumn(name ="idFactura")
    private List<Factura> facturas;

    @OneToMany
    @JoinColumn(name ="IdPlatillos")
    private List<Platillo> platillos;

    @OneToOne
    @JoinColumn(name="id_administrador")
    private Administrador administrador;

    private static final long serialVersionUID = -780885112066891233L;

}
