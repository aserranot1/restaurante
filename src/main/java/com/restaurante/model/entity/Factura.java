package com.restaurante.model.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="facturas")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Factura implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "numeroDeFactura",nullable = true,unique = true)
    private int numeroFactura;

    @Column(name="clientes")
    private String nombreCliente;

    private long identificacionCliente;

    @OneToMany
    @JoinColumn(name ="idPlatillos")
    private List<Platillo> platillos;

    //Temporal(TemporalType.DATE)
    private LocalDate fecha;

    private double total;

    private double pago;

    private double cambio;
/*
    public Factura(String nombreCliente, long identificacionCliente, List<Platillo> platillos, Date fecha, double pago) {
        this.nombreCliente = nombreCliente;
        this.identificacionCliente = identificacionCliente;
        this.platillos = platillos;
                for (int i = 0; i<platillos.size(); i++){
        this.total=this.getTotal()+platillos.get(i).getPrecio();
        }
        this.pago = pago;
        this.cambio = this.pago-total;
    }
*/
    private static final long serialVersionUID = -327496110748601951L;

}
