package com.restaurante.model.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="platillo")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Platillo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IdPlatillos")
    private int id;

    @Column(name="nombre")
    private String nombrePlatillo;

    private String descripcion;

    private double precio;


    private static final long serialVersionUID = -4935054878270238634L;

}
