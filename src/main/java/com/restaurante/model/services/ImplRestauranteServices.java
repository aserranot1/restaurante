package com.restaurante.model.services;

import com.restaurante.model.entity.Factura;
import com.restaurante.model.entity.Platillo;
import com.restaurante.model.entity.Restaurante;
import com.restaurante.model.repositori.IFacturaDao;
import com.restaurante.model.repositori.IRestauranteDao;
import com.restaurante.model.services.implement.RestauranteServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class ImplRestauranteServices implements RestauranteServices {

    @Autowired
    public IRestauranteDao restauranteDao;
    public IFacturaDao facturaDao;

    public Restaurante restaurante;

    @Override
    public Restaurante crearRestaurate(int id, String nombre, String sede) {

        List<Platillo> platillos= new ArrayList<Platillo>();
        List<Factura>  facturas = new ArrayList<Factura>();

        restaurante.setIdRestaurante(id);
        restaurante.setNombreRestaurante(nombre);
        restaurante.setSede(sede);
        restaurante.setPlatillos(platillos);
        restaurante.setFacturas(facturas);

        return restauranteDao.save(restaurante);

    }

    @Override
    public Restaurante modifcarRestaurante(String nombre, String sede) {

        restaurante.setNombreRestaurante(nombre);
        restaurante.setSede(sede);

        return restauranteDao.save(restaurante);
    }

    @Override
    public void eliminarRestaurante() {
    restauranteDao.delete(restaurante);
    }

    @Override
    public Restaurante mostrarRestaurante() {

        return (Restaurante) restauranteDao.findAll();
    }

    @Override
    public void agregarFactura(Factura factura) {
        restaurante.getFacturas().add(factura);
        restauranteDao.save(restaurante);
    }

    @Override
    public void modificarFactura(Factura factura ) {



    }

    @Override
    public void eliminarFactura(int id) {

    }

    @Override
    public List<Factura> mostrarFacturas() {
        return null;
    }

    @Override
    public Factura buscarFacturaPorCriterio(int nuemeroFactura) {
        int id;



        return null;
    }

    @Override
    public Factura buscarFacturaPorCriterio(String Cliente) {
        return null;
    }

    @Override
    public Factura buscarFacturaPorCriterio(LocalDate fecha) {
        return null;
    }

    @Override
    public void agregarPlatillos(int id, String nombre, String sede) {

    }

    @Override
    public void modifcarPlatillo(String nombre, String sede) {

    }

    @Override
    public void eliminarPlatillo(int id) {

    }

    @Override
    public List<Platillo> mostrarPlatillos() {
        return null;
    }

    @Override
    public Platillo buscarPlatilloPorCriterio(int id) {
        return null;
    }

    @Override
    public Platillo buscarPlatilloPorCriterio(String nombrePlatillo) {
        return null;
    }

    @Override
    public Platillo buscarPlatilloPorCriterio(double precio) {
        return null;
    }

}
