package com.restaurante.model.services;

import com.restaurante.exception.exceptions.MasterResourceNotFoundException;
import com.restaurante.model.entity.Administrador;
import com.restaurante.model.repositori.IAdministradorDao;
import com.restaurante.model.services.implement.AdministradorServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
public class ImplAdministradorServices implements AdministradorServices {
    @Autowired
    IAdministradorDao administradorDao;
    Administrador administrador;

    @Override
    public Administrador crearAdministrador(long identificacion, String nombre) {

        if(administrador==null){
            administrador.setNombre(nombre);
            administrador.setIdentificacion(identificacion);
            return administradorDao.save(administrador);
        }

        return administradorDao.save(administrador);
    }

    @Override
    public Administrador modificarAdministrador(long id,String nombre) {
        if(administrador==null){
            administrador.setNombre(nombre);
            administrador.setIdentificacion(id);
            return administradorDao.save(administrador);
        }

        return administradorDao.save(administrador);
            }

    @Override
    public void eliminarAdministrador() {
     administradorDao.delete(administrador);
    }

    @Override
    public Administrador mostrarAdministrador() {
        return (Administrador) administradorDao.findAll();
    }
}

