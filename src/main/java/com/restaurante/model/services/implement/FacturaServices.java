package com.restaurante.model.services.implement;

import com.restaurante.model.entity.Administrador;
import com.restaurante.model.entity.Factura;
import com.restaurante.model.entity.Platillo;
import com.restaurante.model.entity.Restaurante;

import java.util.List;

public interface FacturaServices {

    public Factura       crearFactura(String nombreCliente, long idCliente, double pago);
    public Factura       modificarFactura(String nombreCliente, long idCliente, double pago);




}
