package com.restaurante.model.services.implement;

import com.restaurante.exception.exceptions.MasterResourceNotFoundException;
import com.restaurante.model.entity.Administrador;


import java.util.List;

public interface AdministradorServices {

    public Administrador crearAdministrador(long identificacion, String nombre);
    public Administrador modificarAdministrador(long id,String nombre);
    public void          eliminarAdministrador();
    public Administrador mostrarAdministrador();



}
