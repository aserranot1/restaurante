package com.restaurante.model.services.implement;

import com.restaurante.model.entity.Administrador;
import com.restaurante.model.entity.Platillo;
import com.restaurante.model.entity.Restaurante;

import java.util.List;

public interface PlatillosServices {


    public Platillo       crearPlatillo(String nombre, String descripcion, double precio);
    public Platillo       modifcarPlatillo(String nombre, String descripcion, double precio);


}
