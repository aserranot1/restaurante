package com.restaurante.model.services.implement;


import com.restaurante.model.entity.Administrador;
import com.restaurante.model.entity.Factura;
import com.restaurante.model.entity.Platillo;
import com.restaurante.model.entity.Restaurante;

import java.time.LocalDate;
import java.util.List;

public interface RestauranteServices {

    public Restaurante    crearRestaurate(int id,String nombre, String sede);
    public Restaurante    modifcarRestaurante(String nombre, String sede);
    public void           eliminarRestaurante();
    public Restaurante    mostrarRestaurante();

    public void           agregarFactura(Factura factura);
    public void           modificarFactura(Factura factura);
    public void           eliminarFactura(int id);
    public List<Factura>  mostrarFacturas();
    public Factura        buscarFacturaPorCriterio(int nuemeroFactura);
    public Factura        buscarFacturaPorCriterio(String Cliente);
    public Factura        buscarFacturaPorCriterio(LocalDate fecha);

    public void           agregarPlatillos(int id,String nombre, String sede);
    public void           modifcarPlatillo(String nombre, String sede);
    public void           eliminarPlatillo(int id);
    public List<Platillo> mostrarPlatillos();
    public Platillo       buscarPlatilloPorCriterio(int id);
    public Platillo       buscarPlatilloPorCriterio(String nombrePlatillo);
    public Platillo       buscarPlatilloPorCriterio(double precio);


}
