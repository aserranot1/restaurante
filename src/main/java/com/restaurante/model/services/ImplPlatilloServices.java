package com.restaurante.model.services;

import com.restaurante.model.entity.Platillo;
import com.restaurante.model.repositori.IAdministradorDao;
import com.restaurante.model.repositori.IPlatilloDao;
import com.restaurante.model.services.implement.PlatillosServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ImplPlatilloServices implements PlatillosServices {
   @Autowired
    public IPlatilloDao platilloDao;
    public Platillo platillo;


    @Override
    public Platillo crearPlatillo(String nombre, String descripcion, double precio) {

        platillo.setNombrePlatillo(nombre);
        platillo.setPrecio(precio);
        platillo.setDescripcion(descripcion);

    return platilloDao.save(platillo);
    }

    @Override
    public Platillo modifcarPlatillo(String nombre, String descripcion, double precio) {

        platillo.setNombrePlatillo(nombre);
        platillo.setPrecio(precio);
        platillo.setDescripcion(descripcion);

        return platilloDao.save(platillo);
    }
    }


