package com.restaurante.model.services;


import com.restaurante.model.entity.Administrador;
import com.restaurante.model.entity.Factura;
import com.restaurante.model.entity.Platillo;
import com.restaurante.model.repositori.IFacturaDao;
import com.restaurante.model.services.implement.FacturaServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
@Service
public class ImplFacturaServices implements FacturaServices {
     @Autowired
    public IFacturaDao facturaDao;
    public Factura factura;

    @Override
    public Factura crearFactura(String nombreCliente, long idCliente, double pago) {
        List<Platillo> platillos= null;
        platillos.add(new Platillo());
        double total=0;

        factura.setNombreCliente(nombreCliente);
        factura.setIdentificacionCliente(idCliente);
        factura.setPago(pago);
        factura.setPlatillos(platillos);
        factura.setFecha(LocalDate.now());
        if(factura.getPlatillos()!=null){
            for (int i=0;i<factura.getPlatillos().size();i++) {
                total = total + factura.getPlatillos().get(i).getPrecio();
            }
            factura.setTotal(total);
        }else{
            factura.setTotal(0);
        }
        factura.setCambio(factura.getPago()-factura.getTotal());

        return facturaDao.save(factura);
    }

    @Override
    public Factura modificarFactura(String nombreCliente, long idCliente, double pago) {
        List<Platillo> platillos= null;
        platillos.add(new Platillo());
        double total=0;

        factura.setNombreCliente(nombreCliente);
        factura.setIdentificacionCliente(idCliente);
        factura.setPago(pago);
        factura.setPlatillos(platillos);
        factura.setFecha(LocalDate.now());
        if(factura.getPlatillos()!=null){
            for (int i=0;i<factura.getPlatillos().size();i++) {
                total = total + factura.getPlatillos().get(i).getPrecio();
            }
            factura.setTotal(total);
        }else{
            factura.setTotal(0);
        }
        factura.setCambio(factura.getPago()-factura.getTotal());

        return facturaDao.save(factura);
    }


}
