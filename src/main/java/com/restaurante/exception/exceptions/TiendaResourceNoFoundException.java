package com.restaurante.exception.exceptions;

public class TiendaResourceNoFoundException extends Exception{
    public static final String DESCRIPCION = "registro no encontrado";

    public TiendaResourceNoFoundException() {
        super(DESCRIPCION);
    }

    public TiendaResourceNoFoundException(String message) {
        super(DESCRIPCION + ": " + message);
    }
}
